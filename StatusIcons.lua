local ET = unpack(select(2,...))
if not ET.Enable then return end

local StatusIcon = GameTooltip:CreateTexture(nil, "ARTWORK")

function ET:SetStatusIcon()
	StatusIcon:Hide()
	local GetMouseFocus = GetMouseFocus()
	local unit = select(2, GameTooltip:GetUnit()) or (GetMouseFocus and GetMouseFocus:GetAttribute("unit"))
	if not unit then unit = "mouseover" end
	if not UnitExists(unit) then return end
	StatusIcon:SetTexCoord(0, 1, 0, 1)
	if GetRaidTargetIndex(unit) then
		StatusIcon:SetTexture(ET.RaidIcons)
		SetRaidTargetIconTexture(StatusIcon, GetRaidTargetIndex(unit))
		StatusIcon:Show()
	elseif UnitIsPlayer(unit) and UnitIsPVPFreeForAll(unit) then
		StatusIcon:SetTexture(ET.FFAIcon)
		StatusIcon:Show()
	elseif UnitIsPlayer(unit) and UnitIsPVP(unit) and UnitFactionGroup(unit) then
		StatusIcon:SetTexture(ET.TexturePath..UnitFactionGroup(unit))
		StatusIcon:Show()
	elseif UnitAffectingCombat(unit) then
		StatusIcon:SetTexture(ET.CombatIcon)
		StatusIcon:Show()
	end
	StatusIcon:SetPoint("CENTER", GameTooltip, "LEFT", 0, 0)
	StatusIcon:SetSize(24, 24)
end

GameTooltip:HookScript("OnShow", ET.SetStatusIcon)