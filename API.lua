local ET = unpack(select(2,...))
if not ET.Enable then return end

local format, select = format, select
local UnitIsPlayer, UnitHasVehicleUI, UnitClass, UnitReaction = UnitIsPlayer, UnitHasVehicleUI, UnitClass, UnitReaction
local RAID_CLASS_COLORS, FACTION_BAR_COLORS = RAID_CLASS_COLORS, FACTION_BAR_COLORS

function ET:GetUnitColor(unit)
	local reaction, color = UnitReaction(unit, "player")
	if UnitIsPlayer(unit) and not UnitHasVehicleUI(unit) then
		color = RAID_CLASS_COLORS[select(2, UnitClass(unit))]
	elseif reaction then
		color = FACTION_BAR_COLORS[reaction]
	end
	if not color then return .6, .6, .6 end
	return color.r, color.g, color.b
end

function ET:ColorGradient(perc, ...)
	if perc >= 1 then
		return select(select('#', ...) - 2, ...)
	elseif perc <= 0 then
		return ...
	end

	local num = select('#', ...) / 3
	local segment, relperc = math.modf(perc*(num-1))
	local r1, g1, b1, r2, g2, b2 = select((segment*3)+1, ...)

	return r1 + (r2-r1)*relperc, g1 + (g2-g1)*relperc, b1 + (b2-b1)*relperc
end

ET.NPCClassification = {
	worldboss = "|cffAF5050Boss|r",
	rareelite = "|cffAF5050Rare Elite|r",
	elite = "|cffAF5050Elite|r",
	rare = "|cffAF5050Rare|r",
	normal = "",
	minus = "",
}

ET.GetPowerColor = {
	["MANA"] = {0.31, 0.45, 0.63},
	["RAGE"] = {0.69, 0.31, 0.31},
	["FOCUS"] = {0.71, 0.43, 0.27},
	["ENERGY"] = {0.65, 0.63, 0.35},
	["RUNES"] = {0.55, 0.57, 0.61},
	["RUNIC_POWER"] = {0, 0.82, 1},
	["AMMOSLOT"] = {0.8, 0.6, 0},
	["FUEL"] = {0, 0.55, 0.5},
	["POWER_TYPE_STEAM"] = {0.55, 0.57, 0.61},
	["POWER_TYPE_PYRITE"] = {0.60, 0.09, 0.17},
	["POWER_TYPE_FEL_ENERGY"] = { .11, .66, .11 },
}

ET.GetArenaNumber = {
	arena1 = 1,
	arena2 = 2,
	arena3 = 3,
	arena4 = 4,
	arena5 = 5,
}