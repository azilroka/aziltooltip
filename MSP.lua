local ET = unpack(select(2,...))
if not ET.Enable then return end

function ET:GetMSPField(player, field)
	if msp ~= nil then
		if (msp.char[player] ~= nil) then
			if (msp.char[player].field[field] ~= "" or msp.char[player].field[field] ~= 0) then
				return msp.char[player].field[field]
			end
		end
	elseif msptrp ~= nil then
		if (msptrp.char[player] ~= nil) then
			if (msptrp.char[player].field[field] ~= "") then
				return msptrp.char[player].field[field]
			end
		end
	else
		return nil
	end
end

function ET:GetCharacterFields(player, field)
	if msp ~= nil then
		msp:Request(player, field)
	end
	if msptrp ~= nil then
		msptrp:Request(player, field)
	end
end

function ET:SetMSPTooltip()
	if not (msp or msptrp) then return end
	local GetMouseFocus = GetMouseFocus()
	local Unit = select(2, GameTooltip:GetUnit()) or GetMouseFocus and GetMouseFocus:GetAttribute("unit")
	if not Unit then Unit = "mouseover" end
	if not UnitIsPlayer(Unit) then return end
	local Name, Realm = UnitName(Unit)
	local Player
	if Realm == select(2, UnitName("player")) then
		Player = Name
	else
		Player = Name.."-"..Realm
	end
	ET:GetCharacterFields(Player, { 'VP', 'VA', 'NA', 'NH', 'NI', 'NT', 'RA', 'FR', 'FC', 'CU' })
	local RPName = ET:GetMSPField(Player, "NA")
	local NickName = ET:GetMSPField(Player, "NI")
	local Style = ET:GetMSPField(Player, "FR")
	local Status = ET:GetMSPField(Player, "FC")
	local Client = ET:GetMSPField(Player, "VA")
	local Currently = ET:GetMSPField(Player, "CU")
	local Description = ET:GetMSPField(Player, "DE")
	Description = Description ~= "" and "Yes" or "No"
	Style = Style == nil and "Not Set" or Style == "1" and "Normal RolePlayer" or Style == "2" and "Casual RolePlayer" or Status == "3" and "Full-Time RolePlayer" or Status == "4" and "Beginner RolePlayer" or Status
	Status = Status == nil and "Not Set" or Status == "1" and "Out of Character" or Status == "2" and "In Character" or Status == "3" and "Looking for Contact" or Status == "4" and "Storyteller" or Status
	MSPTooltip:SetOwner(UIParent, "ANCHOR_NONE")
	if ET:GetMSPField(Player, "NA") ~= nil and ET:GetMSPField(Player, "NA") ~= "" then
		MSPTooltip:ClearLines()
		MSPTooltip:AddLine("Role Play Information", 1, 1, 1)
		MSPTooltip:AddDoubleLine("Name:", RPName, 1, 1, 1, 1, 1, 1)
		MSPTooltip:AddDoubleLine("NickName:", NickName, 1, 1, 1, 1, 1, 1)
		MSPTooltip:AddDoubleLine("Style:", Style, 1, 1, 1, 1, 1, 1)
		MSPTooltip:AddDoubleLine("Status:", Status, 1, 1, 1, 1, 1, 1)
		MSPTooltip:AddDoubleLine("Client:", Client, 1, 1, 1, 1, 1, 1)
		MSPTooltip:AddDoubleLine("Currently:", Currently, 1, 1, 1, 1, 1, 1)
		MSPTooltip:AddDoubleLine("Description:", Description, 1, 1, 1, 1, 1, 1)
		MSPTooltip:SetPoint("TOPRIGHT", GameTooltip, "TOPLEFT", -2, 0)
		MSPTooltip:SetTemplate("Transparent")
		MSPTooltip:Show()
	end
end

local MSPTooltip = CreateFrame("GameTooltip", "MSPTooltip", UIParent, "GameTooltipTemplate")
MSPTooltip:SetTemplate("Transparent")
MSPTooltip:SetFrameLevel(1)
MSPTooltip:Hide()

GameTooltip:HookScript("OnTooltipSetUnit", ET.SetMSPTooltip)
hooksecurefunc(GameTooltip, "FadeOut", function() MSPTooltip:FadeOut() end)