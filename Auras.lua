local ET = unpack(select(2,...))
if not ET.Enable then return end

local format, floor, pairs, tinsert, unpack = format, floor, pairs, tinsert, unpack
local GetMouseFocus, IsInInstance, UnitBuff, UnitDebuff, UnitIsPlayer = GetMouseFocus, IsInInstance, UnitBuff, UnitDebuff, UnitIsPlayer
local TooltipAuras = {}

function ET:CreateAura()
	local Aura = CreateFrame('Frame', nil, GameTooltip)
	Aura:SetSize(24, 24)
	Aura:SetTemplate()
	Aura:SetBackdropColor(0, 0, 0, 0)

	Aura.Count = Aura:CreateFontString(nil, 'OVERLAY')
	Aura.Count:SetPoint('BOTTOMRIGHT', 1, 0)
	Aura.Count:SetFont(GameFontNormal:GetFont(), 12, 'OUTLINE')

	Aura.Icon = Aura:CreateTexture(nil, 'ARTWORK')
	Aura.Icon:SetInside()
	Aura.Icon:SetTexCoord(unpack(ET.TexCoords))

	Aura.Cooldown = CreateFrame('Cooldown', nil, Aura, 'CooldownFrameTemplate')
	Aura.Cooldown:SetReverse(1)
	Aura.Cooldown:SetAllPoints()
	Aura.Cooldown:SetFrameLevel(Aura:GetFrameLevel() + 1)

	tinsert(TooltipAuras, Aura)
	return Aura
end

function ET:ShowAuras(event, unit)
	local GetMouseFocus = GetMouseFocus()
	local unit = select(2, GameTooltip:GetUnit()) or (GetMouseFocus and GetMouseFocus:GetAttribute('unit'))
	if not unit then unit = 'mouseover' end
	local Instance, InstanceType = IsInInstance()
	if UnitIsPlayer(unit) then
		local Position = 1
		local AurasPerRow = floor(GameTooltip:GetWidth() / 24)
		GameTooltip:SetWidth(((24 + 2) * AurasPerRow) - 2)
		local Index = 1
		while true do
			local _, _, Icon, Count, Dispelable, Start, Finish, Caster, Stealable, _, SpellID = UnitBuff(unit, Index, 'CANCELABLE')
			if not Icon or (Position / AurasPerRow > 2) then
				break
			end
			if (Caster == 'player' or Caster == 'pet' or Caster == 'vehicle') or InstanceType == 'arena' or InstanceType == 'pvp' then
				local Aura = TooltipAuras[Position] or ET:CreateAura()
				Aura:ClearAllPoints()
				if ((Position - 1) % AurasPerRow == 0) or (Position == 1) then
					local x, y = 0, ((24 + 1) * floor((Position - 1) / AurasPerRow)) + 2
					Aura:SetPoint('BOTTOMLEFT', GameTooltip, 'TOPLEFT', x, y)
				else
					Aura:SetPoint('LEFT', TooltipAuras[Position - 1], 'RIGHT', 2, 0)
				end
				Aura.Cooldown:SetCooldown(Finish - Start, Start)
				Aura.Icon:SetTexture(Icon)
				Aura.Count:SetText(Count and Count > 1 and Count or '')
				if Dispelable then
					Aura:SetBackdropBorderColor(1, 1, 0)
				else
					Aura:SetBackdropBorderColor(.6, .6, .6)
				end
				Aura:Show()
				Position = Position + 1
			end
			Index = Index + 1
		end
		local Index = 1
		local BuffCount = Position - 1
		while true do
			local _, _, Icon, Count, Dispelable, Start, Finish, Caster, Stealable, _, SpellID = UnitDebuff(unit, Index)
			if not Icon or (Position / AurasPerRow > 2) then
				break
			end
			if (Caster == 'player' or Caster == 'pet' or Caster == 'vehicle') or InstanceType == 'arena' or InstanceType == 'pvp' then
				local Aura = TooltipAuras[Position] or ET:CreateAura()
				Aura:ClearAllPoints()
				if ((Position - 1) % AurasPerRow == 0) or (Position == BuffCount + 1) then
					local x, y = 1, ((24 + 1) * floor((Position - 1) / AurasPerRow)) + 2
					Aura:SetPoint('BOTTOMRIGHT', GameTooltip, 'TOPRIGHT', x, y)
				else
					Aura:SetPoint('RIGHT', TooltipAuras[Position - 1], 'LEFT', -1, 0)
				end
				Aura.Cooldown:SetCooldown(Finish - Start, Start)
				Aura.Icon:SetTexture(Icon)
				Aura.Count:SetText(Count and Count > 1 and Count or '')
				if Dispelable then
					Aura:SetBackdropBorderColor(1, 1, 0)
				else
					Aura:SetBackdropBorderColor(1, 0, 0)
				end
				Aura:Show()
				Position = Position + 1
			end
			Index = Index + 1
		end
		for i = Position, #TooltipAuras do
			TooltipAuras[i]:Hide()
		end
	end
end

function ET:HideAuras()
	for k, v in pairs(TooltipAuras) do
		TooltipAuras[k]:Hide()
	end
end

GameTooltip:HookScript('OnHide', ET.HideAuras)
GameTooltip:RegisterUnitEvent('UNIT_AURA', 'mouseover', 'target')
GameTooltip:HookScript('OnEvent', ET.ShowAuras)