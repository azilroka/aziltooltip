local ET = unpack(select(2,...))
if not ET.Enable then return end

local format, select, strsub, strfind, strmatch, tonumber = format, select, strsub, strfind, strmatch, tonumber
local GetMouseFocus, UnitExists, UnitRace, UnitClass, UnitLevel, UnitPVPName, UnitSex = GetMouseFocus, UnitExists, UnitRace, UnitClass, UnitLevel, UnitPVPName, UnitSex
local GetGuildInfo, UnitName, UnitCreatureType, UnitClassification, GetQuestDifficultyColor = GetGuildInfo, UnitName, UnitCreatureType, UnitClassification, GetQuestDifficultyColor
local UnitIsAFK, UnitIsDND, UnitIsDeadOrGhost, UnitIsInMyGuild, UnitGUID = UnitIsAFK, UnitIsDND, UnitIsDeadOrGhost, UnitIsInMyGuild, UnitGUID

function ET:SetGameTooltip()
	local GetMouseFocus = GetMouseFocus()
	local unit = select(2, GameTooltip:GetUnit()) or GetMouseFocus and GetMouseFocus:GetAttribute('unit')
	if not UnitExists(unit) then return end

	local Race, Class, Level, Title, Gender = UnitRace(unit), UnitClass(unit), UnitLevel(unit), UnitPVPName(unit), UnitSex(unit)
	local Guild, GuildRank = GetGuildInfo(unit)
	local Name, Realm = UnitName(unit)
	local CreatureType, Classification = UnitCreatureType(unit), UnitClassification(unit)
	Gender = Gender == 2 and MALE or Gender == 3 and FEMALE
	local QR, QG, QB = GetQuestDifficultyColor(Level).r, GetQuestDifficultyColor(Level).g, GetQuestDifficultyColor(Level).b
	local UR, UG, UB, UnitHex = ET:GetUnitColor(unit)

	if UnitIsPlayer(unit) then
		GameTooltip:ClearLines()
		local RightString, RSR, RSG, RSB = '', 1, 1, 1
		if UnitIsAFK(unit) then
			RightString, RSR, RSG, RSB = CHAT_FLAG_AFK, 1, 1, 0
		elseif UnitIsDND(unit) then 
			RightString, RSR, RSG, RSB = CHAT_FLAG_DND, .77, .12, .23
		elseif UnitIsDeadOrGhost(unit) then
			RightString, RSR, RSG, RSB = DEAD, .77, .12, .23
		end
		GameTooltip:AddDoubleLine(Title, RightString, UR, UG, UB, RSR, RSG, RSB)

		if Guild then
			local R, G, B = 0, 1, .06
			if UnitIsInMyGuild(unit) then R, G, B = 0, .56, 1 end
			GameTooltip:AddDoubleLine(format('|cFFFFFFFF<|r%s|cFFFFFFFF>|r', Guild), format('|cFFFFFFFF[|r%s|cFFFFFFFF]|r', GuildRank), R, G, B, 0, 1, .06)
		end
		if Level > 0 then
			GameTooltip:AddDoubleLine(format('%s |cFFFFFFFF%s %s', Level, Race, Class), Gender, QR, QG, QB, 1, 1, 1)
		else
			GameTooltip:AddDoubleLine(format('?? |cFFFFFFFF%s %s', Race, Class), Gender, QR, QG, QB, 1, 1, 1)
		end
	else
		GameTooltip:ClearLines()
		GameTooltip:AddLine(Title, UR, UG, UB)
		if(Level == -1 and Classification == 'elite') then
			Classification = 'worldboss'
		end
		if Level and Level > 0 then
			GameTooltip:AddDoubleLine(format('%s |cFFFFFFFF%s', Level, ET.NPCClassification[Classification]), CreatureType, QR, QG, QB, 1, 1, 1)
		else
			GameTooltip:AddDoubleLine(format('?? |cFFFFFFFF%s', ET.NPCClassification[Classification]), CreatureType,  QR, QG, QB, 1, 1, 1)
		end
		GameTooltip:AddDoubleLine('NPC ID', tonumber(strsub(UnitGUID(unit), -12, -9), 16), 1, 1, 1, 1, 1, 1)
	end

	local Target = UnitName(format('%starget', unit))
	if Target then
		local R, G, B
		if Target == UnitName('player') then
			Target, R, G, B = 'YOU', .8, .3, .3
		else
			R, G, B = ET:GetUnitColor(format('%starget', unit))
		end

		GameTooltip:AddDoubleLine('Targetting', Target, 1, 1, 1, R, G, B)
	end
end

function ET:ColorTooltip()
	local GetMouseFocus = GetMouseFocus()
	local unit = select(2, GameTooltip:GetUnit()) or GetMouseFocus and GetMouseFocus:GetAttribute('unit')
	if not unit then unit = 'mouseover' end
	local R, G, B, Hex = ET:GetUnitColor(unit)
	GameTooltip:SetTemplate('Transparent')
	GameTooltip:SetBackdropBorderColor(R, G, B)
end

GameTooltip:HookScript('OnShow', ET.ColorTooltip)
-- GameTooltip:HookScript('OnUpdate', function(self)
	-- local Font, FontSize, FontFlag = Tukui[2]["Media"].Font, 12, "OUTLINE"
	-- for i = 1, GameTooltip:NumLines() do
		-- _G['GameTooltipTextLeft'..i]:SetFont(Font, FontSize, FontFlag)
		-- _G['GameTooltipTextRight'..i]:SetFont(Font, FontSize, FontFlag)
	-- end
-- end)
GameTooltip:HookScript('OnTooltipSetUnit', ET.SetGameTooltip)

if IsAddOnLoaded('ElvUI') then return end
hooksecurefunc(GameTooltip, 'SetUnitBuff', function(self, ...)
	local id = select(11, UnitBuff(...))
	if not id then return end
	self:AddLine(' ')
	self:AddDoubleLine('Spell ID', id, 1, 0, 0, 1, 1, 1)
	self:Show()
end)

hooksecurefunc(GameTooltip, 'SetUnitDebuff', function(self, ...)
	local id = select(11, UnitDebuff(...))
	if not id then return end
	self:AddLine(' ')
	self:AddDoubleLine('Spell ID', id, 1, 0, 0, 1, 1, 1)
	self:Show()
end)

hooksecurefunc(GameTooltip, 'SetUnitAura', function(self, ...)
	local id = select(11, UnitAura(...))
	if not id then return end
	self:AddLine(' ')
	self:AddDoubleLine('Spell ID', id, 1, 0, 0, 1, 1, 1)
	self:Show()
end)

hooksecurefunc('SetItemRef', function(link)
	if strfind(link, '^spell:') then
		local id = strsub(link, 7)
		ItemRefTooltip:AddLine(' ')
		ItemRefTooltip:AddDoubleLine('Spell ID', id, 1, 0, 0, 1, 1, 1)
		ItemRefTooltip:Show()
	end
	if strfind(link, '^item:') then
		local id = strmatch(link, ':(%w+)')
		ItemRefTooltip:AddLine(' ')
		ItemRefTooltip:AddDoubleLine('Item ID', id, 1, 0, 0, 1, 1, 1)
--		ItemRefTooltip:AddDoubleLine('Item Count', GetItemCount(link), 1, 0, 0, 1, 1, 1)
		ItemRefTooltip:Show()
	end
end)

GameTooltip:HookScript('OnTooltipSetSpell', function(self)
	local id = select(3, self:GetSpell())
	if not id then return end
	local lines = self:NumLines()
	local isFound
	for i= 1, lines do
		local line = _G[('GameTooltipTextLeft%d'):format(i)]
		if line and line:GetText() and strfind(line:GetText(), 'Spell ID') then
			isFound = true
			break
		end
	end

	if not isFound then
		self:AddLine(' ')
		self:AddDoubleLine('Spell ID', id, 1, 0, 0, 1, 1, 1)
		self:Show()
	end
end)

GameTooltip:HookScript('OnTooltipSetItem', function(self)
	local item, link = self:GetItem()
	if not link then return end
	local id = strmatch(link, ':(%w+)')
	self:AddLine(' ')
	self:AddDoubleLine('Item ID', id, 1, 0, 0, 1, 1, 1)
	--self:AddDoubleLine('Item Count', GetItemCount(link), 1, 0, 0, 1, 1, 1)
	self:Show()
end)