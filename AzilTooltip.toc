## Interface: 50300
## Title: |cFFFF7D0AAzil|r GameTooltip
## Author: Azilroka
## Version: 1.00
## OptionalDeps: Tukui, ElvUI, DuffedUI, AsphyxiaUI

Init.lua
API.lua
GameTooltip.lua
Auras.lua
StatusIcons.lua
StatusBars.lua
MSP.lua
Inspect.lua