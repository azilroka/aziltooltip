local ET = unpack(select(2,...))
if not ET.Enable then return end

local format, gsub, select = format, gsub, select
local GetMouseFocus, UnitExists, UnitIsTapped, UnitIsTappedByPlayer = GetMouseFocus, UnitExists, UnitIsTapped, UnitIsTappedByPlayer
local UnitHealth, UnitHealthMax, UnitPower, UnitPowerMax, UnitPowerType, UnitDetailedThreatSituation = UnitHealth, UnitHealthMax, UnitPower, UnitPowerMax, UnitPowerType, UnitDetailedThreatSituation

function ET:CreateStatusBar(Name)
	local Frame = CreateFrame('StatusBar', 'GameTooltip_'..Name, GameTooltip)
	Frame.Text = Frame:CreateFontString(nil, 'OVERLAY')
	Frame.Text:SetPoint('CENTER', Frame)
	Frame:Hide()
	Frame:CreateBackdrop('Transparent')
	Frame:SetStatusBarTexture(ET.StatusBar)
	Frame:SetSize(GameTooltip:GetWidth() - (ElvUI and ElvUI[1].PixelMode and 2 or 4), 12)
	Frame:SetScript('OnUpdate', function(f) f:SetSize(GameTooltip:GetWidth() - (ElvUI and ElvUI[1].PixelMode and 2 or 4), 12) end)

	return Frame
end

local HealthBar, HealthBarLine, HealthBarLineText = ET:CreateStatusBar('Health'), nil, HEALTH
local PowerBar, PowerBarLine, PowerBarLineText = ET:CreateStatusBar('Power'), nil, POWER
local ThreatBar, ThreatBarLine, ThreatBarLineText = ET:CreateStatusBar('Threat'), nil, THREAT

function ET:ShortValue(v)
	if (v >= 1e6) then
		return gsub(format('%.1fm', v / 1e6), '%.?0+([km])$', '%1')
	elseif (v >= 1e3 or v <= -1e3) then
		return gsub(format('%.1fk', v / 1e3), '%.?0+([km])$', '%1')
	else
		return v
	end
end

local Point, RelativeTo, RelativePoint, xOffset, yOffset, yNewOffset = nil, nil, nil, nil, nil, 0
function ET:SetStatusBars(event)
	if event ~= 'StatusBarShow' and event ~= 'UNIT_HEALTH' and event ~= 'UNIT_POWER' and event ~= 'UNIT_THREAT_SITUATION_UPDATE' and event ~= 'UNIT_COMBAT' and event ~= 'UNIT_TARGET' then return end
	local GetMouseFocus = GetMouseFocus()
	local unit = select(2, GameTooltip:GetUnit()) or (GetMouseFocus and GetMouseFocus:GetAttribute('unit'))
	if not unit then unit = 'mouseover' end
	if not UnitExists(unit) then return end
	local Health, HealthMax = UnitHealth(unit), UnitHealthMax(unit)
	local PowerType, Power, PowerMax = select(2, UnitPowerType(unit)), UnitPower(unit), UnitPowerMax(unit)
	local Threat = select(3, UnitDetailedThreatSituation(unit, 'target'))
	local Font, FontSize, FontFlag = GameTooltipTextLeft1:GetFont()
	local StatusBaryOffset = ElvUI and ElvUI[1].PixelMode and -3 or -6
	local r, g, b, tr, tg, tb, Color
	GameTooltipStatusBar:Hide()
	yNewOffset = 0
	if Health and Health > 0 then
		HealthBar:Show()
		HealthBar:SetMinMaxValues(0, HealthMax)
		HealthBar:SetValue(Health)
		HealthBar.Text:SetFont(Font, FontSize, FontFlag)
		HealthBar.Text:SetText(format('%s / %s', ET:ShortValue(Health), ET:ShortValue(HealthMax)))
		if UnitIsTapped(unit) and not UnitIsTappedByPlayer(unit) then
			r, g, b = .6, .6, .6
		else
			r, g, b = ET:ColorGradient((Health/HealthMax), .8, 0, 0, .8, .8, 0, 0, .8, 0)
		end
		HealthBar:SetStatusBarColor(r, g, b)
		HealthBar:SetPoint('TOP', GameTooltip, 'BOTTOM', 0, ElvUI and ElvUI[1].PixelMode and -2 or -4)
		yNewOffset = yNewOffset + HealthBar:GetHeight()
	else
		HealthBar:Hide()
	end
	if Power and Power > 0 or UnitIsPlayer(unit) and PowerType == 'RAGE' or UnitIsPlayer(unit) and PowerType == 'RUNIC_POWER' then
		PowerBar:Show()
		PowerBar:SetMinMaxValues(0, PowerMax)
		PowerBar:SetValue(Power)
		PowerBar.Text:SetFont(Font, FontSize, FontFlag)
		PowerBar.Text:SetText(format('%s / %s', ET:ShortValue(Power), ET:ShortValue(PowerMax)))
		Color = ET.GetPowerColor[PowerType]
		PowerBar:SetStatusBarColor(Color[1], Color[2], Color[3])
		PowerBar:SetPoint('TOP', HealthBar, 'BOTTOM', 0, StatusBaryOffset)
		yNewOffset = yNewOffset + PowerBar:GetHeight()
	else
		PowerBar:Hide()
	end
	if Threat and Threat > 0 then
		ThreatBar:Show()
		ThreatBar:SetMinMaxValues(0, 100)
		ThreatBar:SetValue(Threat)
		ThreatBar.Text:SetFont(Font, FontSize, FontFlag)
		ThreatBar.Text:SetFormattedText('%3.1f', Threat)
		tr, tg, tb = ET:ColorGradient(Threat, 100, 0, .8, 0, .8, .8, 0, .8, 0, 0)
		ThreatBar:SetStatusBarColor(tr, tg, tb)
		ThreatBar:SetPoint('TOP', PowerBar, 'BOTTOM', 0, StatusBaryOffset)
		yNewOffset = yNewOffset + ThreatBar:GetHeight()
	else
		ThreatBar:Hide()
	end
	if yOffset ~= nil then
		GameTooltip:ClearAllPoints()
		GameTooltip:SetPoint(Point, RelativeTo, RelativePoint, xOffset, yOffset + yNewOffset)
	end
end

function ET:HideStatusBars()
	HealthBar:Hide()
	ThreatBar:Hide()
	PowerBar:Hide()
	HealthBarLine, ThreatBarLine, PowerBarLine, Point, RelativeTo, RelativePoint, xOffset, yOffset = nil, nil, nil, nil, nil, nil, nil, nil
end

GameTooltip:HookScript('OnTooltipSetUnit', function() ET:SetStatusBars('StatusBarShow') end)
GameTooltip:HookScript('OnShow', function() Point, RelativeTo, RelativePoint, xOffset, yOffset = GameTooltip:GetPoint() end)
GameTooltip:RegisterUnitEvent('UNIT_COMBAT', 'target', 'mouseover')
GameTooltip:RegisterUnitEvent('UNIT_HEALTH', 'target', 'mouseover')
GameTooltip:RegisterUnitEvent('UNIT_POWER', 'target', 'mouseover')
GameTooltip:RegisterUnitEvent('UNIT_THREAT_SITUATION_UPDATE', 'target', 'mouseover')
GameTooltip:HookScript('OnEvent', ET.SetStatusBars)
GameTooltip:HookScript('OnHide', ET.HideStatusBars)