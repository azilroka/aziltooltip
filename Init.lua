local AddOnName, Engine = ...
local AddOn = {}

Engine[1] = AddOn
EnhTooltip = Engine

AddOn.Enable = true
AddOn.Version = GetAddOnMetadata(AddOnName, "Version")
AddOn.Title = select(2, GetAddOnInfo(AddOnName))
AddOn.TexturePath = "Interface\\AddOns\\"..AddOnName.."\\Textures\\"
AddOn.RaidIcons = AddOn.TexturePath.."RaidIcons"
AddOn.FFAIcon = AddOn.TexturePath.."FFA"
AddOn.CombatIcon = AddOn.TexturePath.."CombatSwords"
AddOn.StatusBar = AddOn.TexturePath.."StatusBar"
AddOn.BossIcon = AddOn.TexturePath.."Boss"
AddOn.TexCoords = { .1, .9, .1, .9 }

local ConflictAddOns = {
	TipTac,
	TipTacItemRef,
	TipTacOptions,
	TipTacTalents,
}

for k, v in pairs(ConflictAddOns) do
	if IsAddOnLoaded(v) then
		AddOn.Enable = false
	end
end

if not AddOn.Enable then
	local Frame = CreateFrame("Frame")
	Frame:RegisterEvent("PLAYER_ENTERING_WORLD")
	Frame:SetScript("OnEvent", function() print("AzilTooltip Disabled. Please Disable TipTac.") end)
end