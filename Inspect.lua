local ET = unpack(select(2,...))
if not ET.Enable then return end

local format, floor, strmatch = format, floor, strmatch
local GetMouseFocus, IsInInstance, GetInventoryItemLink, GetItemInfo, GetItemQualityColor = GetMouseFocus, IsInInstance, GetInventoryItemLink, GetItemInfo, GetItemQualityColor
local GetSpecializationInfoByID, GetInspectSpecialization, GetArenaOpponentSpec = GetSpecializationInfoByID, GetInspectSpecialization, GetArenaOpponentSpec

function ET:GetActualItemLevel(ItemLink, ItemLevel)
	local Adjust = {
		['0'] = 0,
		['1'] = 8,
		['373'] = 4,
		['374'] = 8,
		['375'] = 4,
		['376'] = 4,
		['377'] = 4,
		['379'] = 4,
		['380'] = 4,
		['445'] = 0,
		['446'] = 4,
		['447'] = 8,
		['451'] = 0,
		['452'] = 8,
		['453'] = 0,
		['454'] = 4,
		['455'] = 8,
		['456'] = 0,
		['457'] = 8,
		['458'] = 0,
		['459'] = 4,
		['460'] = 8,
		['461'] = 12,
		['462'] = 16,
		['465'] = 0,
		['466'] = 4,
		['467'] = 8,
		['476'] = 0,
		['479'] = 0,
	}

	local Upgrade = strmatch(ItemLink, ':(%d+)\124h%[')
	if ItemLevel and Upgrade then
		return ItemLevel + Adjust[Upgrade]
	else
		return ItemLevel
	end
end

local Point, RelativeTo, RelativePoint, xOffset, yOffset
function ET:GatherInfo(event)
	if event ~= 'INSPECT_READY' then return end
	local GetMouseFocus = GetMouseFocus()
	local unit = select(2, GameTooltip:GetUnit()) or (GetMouseFocus and GetMouseFocus:GetAttribute('unit'))
	if not unit then unit = 'mouseover' end
	local Instance, InstanceType = IsInInstance()
	local Text, SR, SG, SB, EQR, EQG, EQB, EQAverage, EQDivide, EQQuality = 'Loading...', 0, 0, 0, 0, 0, 0, 0, 0, 0
	local Font, FontSize, FontFlag = GameTooltipTextLeft1:GetFont()
	for i = 1, 18 do
		local ItemLink = GetInventoryItemLink(unit, i)
		if ItemLink ~= nil then
			local _, _, Quality, ItemLevel = GetItemInfo(ItemLink)
			if ItemLevel ~= nil then
				ItemLevel = ET:GetActualItemLevel(ItemLink, ItemLevel)
				EQDivide = EQDivide + 1
				EQAverage = EQAverage + ItemLevel
				if EQQuality < Quality then
					EQQuality = Quality
				end
			end
		end
	end
	EQAverage = floor(EQAverage / EQDivide)
	EQR, EQG, EQB = GetItemQualityColor(EQQuality)
	local Spec = InstanceType == 'arena' and GetArenaOpponentSpec(GetArenaNumber[unit]) or GetInspectSpecialization(unit)
	if not Spec or Spec == 0 then
		Text = 'No Talents'
		SR, SG, SB = .8, .8, .8
	else
		local _, Name, _, Icon, _, Role = GetSpecializationInfoByID(Spec)
		Text = Name
		if Role == 'DAMAGER' then
			SR, SG, SB = .77, .12, .23
		elseif Role == 'HEALER' then
			SR, SG, SB = .11, .66, .11
		elseif Role == 'TANK' then
			SR, SG, SB = .24, .54, .78
		end
	end
	if not GameTooltip.LinesAdded then
		GameTooltip:AddDoubleLine(SPECIALIZATION, Text, 1, 1, 1, SR, SG, SB)
		GameTooltip:AddDoubleLine('Equipped Item Level', EQAverage, 1, 1, 1, EQR, EQG, EQB)
		GameTooltip.LinesAdded = true
	end
	local Font, FontSize, FontFlag = GameTooltipTextLeft1:GetFont()
	for i = 2, GameTooltip:NumLines() do
		if strfind(i, SPECIALIZATION) then
			_G['GameTooltipTextRight'..i]:SetText(Text, r, g, b)
			_G['GameTooltipTextLeft'..i]:SetFont(Font, FontSize, FontFlag)
			_G['GameTooltipTextRight'..i]:SetFont(Font, FontSize, FontFlag)
		end
	end
	GameTooltip:Show()
	GameTooltip:ClearAllPoints()
	GameTooltip:SetPoint(Point, RelativeTo, RelativePoint, xOffset, yOffset + (FontSize * 2))
	GameTooltip:UnregisterEvent(event)
end

function ET:InspectPlayer()
	local GetMouseFocus = GetMouseFocus()
	local unit = select(2, GameTooltip:GetUnit()) or (GetMouseFocus and GetMouseFocus:GetAttribute('unit'))
	if not unit then unit = 'mouseover' end
	if not UnitIsPlayer(unit) then return end
	GameTooltip:RegisterEvent('INSPECT_READY')
	GameTooltip:HookScript('OnEvent', ET.GatherInfo)
	NotifyInspect(unit)
end

GameTooltip:HookScript('OnShow', function()	Point, RelativeTo, RelativePoint, xOffset, yOffset = GameTooltip:GetPoint() end)
GameTooltip:HookScript('OnTooltipSetUnit', ET.InspectPlayer)
GameTooltip:HookScript('OnHide', function(self) self.LinesAdded = nil end)